# Code Challenge - Nucleus.eti

Projeto desenvolvido para o desafio da vaga de ***Desenvolvedor Full-Stack Laravel*** passado pela empresa **Nucleus.eti**.<br>
O projeto consiste de uma aplicação web para o gerenciamento de faturas/contas que o usuário tenha servindo como um lembrete para saber quando irá ter que paga-las ou quem sabe receber caso preste serviço autônomo.

#### Ferramentas Utilizadas 

+ ***Laravel**: <p>Usada para o backend da aplicação (controllers, Models)*
+ ***Vue 3**: <p>Usada para o frontend da aplicação (views)*
+ ***Jetstream**: <p>Usada para as altenticações de usuario da aplicação (Auth)*
+ ***Inertia**: <p>Usada para o gerenciamento de rota via ***Ziggys*** e comunicação entre o front e o back da aplicação (Route)*
+ ***Bootstrap**: <p>Usada para o controle de estilos da aplicação (CSS)*
+ ***ClearDB**: <p>Banco de dados mysql utilizado na aplicação no Heroku. No ambiente local foi usado o *phpmyadmin* com o ***XAMPP*** para o controle do banco (DB)*


## Acesso Ao Projeto!

Você pode utilizar o projeto como usuário clicando **[aqui!](https://cc-nucleus-app.herokuapp.com/)** ;)

Ou caso você seja desenvolvedor e queira estudar ou analizar o projeto basta baixar a pasta `cc-app` ou clonar o projeto pro seu repositório local. Feito isso siga as instruções de instalação abaixo.

### Instalação

Após ter baixado ou clonado o projeto, abra ele no seu editor de texto, recomendo o *[Visual Studio Code](https://code.visualstudio.com/)*, pois ele tem um terminal integrado que você vai precisar além de também ser um excelente editor de texto. Em seguida baixe e instale as seguintes ferramentas: *[nodeJs](https://nodejs.org/en/), [XAMPP](https://www.apachefriends.org/pt_br/index.html), [Composer](https://getcomposer.org/)*. Depois disso vamos começar!

Abra o arquivo *`package.json`* dentro da `cc-app` e exclua a seguinte linha dentro da tag scripts:<br>
**obs:** Estas linhas são apenas para o deploy no heroku!

```json
  "postinstall": "npm run production" 

```
e do arquivo *`composer.json`*:
```
"post-install-cmd": [
            "@php artisan migrate --force",
            "@php artisan storage:link"
        ]
```

Agora abra o terminal e digite:
**SEMPRE SEM AS ÁSPAS!**

```
  "cd ./cc-app" //caso não esteja na pasta main
  "composer install"

```
para instalar as dependencias do laravel e depois para as do node.

```
  "npm install"

```
Já as dependências instaladas abra o XAMPP e ative o mysql para o banco e o apache pro servidor php

agora devemos criar o nosso arquivo `.env` dentro da pasta `cc-app`. Esse arquivo é responsavel por guardar as configurações sensiveis da nossa aplicação

>**.env exemplo** 
```
APP_NAME=Laravel
APP_ENV=local
APP_KEY=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack
LOG_DEPRECATIONS_CHANNEL=null
LOG_LEVEL=debug

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=

BROADCAST_DRIVER=log
CACHE_DRIVER=file
FILESYSTEM_DISK=local
QUEUE_CONNECTION=sync
SESSION_DRIVER=database
SESSION_LIFETIME=120

MEMCACHED_HOST=127.0.0.1

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_MAILER=smtp
MAIL_HOST=mailhog
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS="hello@example.com"
MAIL_FROM_NAME="${APP_NAME}"

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=
AWS_USE_PATH_STYLE_ENDPOINT=false

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"
```

então vamos gerar a nossa app_key através do comando:
```
php artisan key:generate
```

Calma que agora ta quase tudo pronto, só precisamos criar o nosso banco no phpmyadmin e em seguida gerar as migrations, além de...

Para acessar o phpMyAdmin digite no seu browser:
```
http://localhost/phpmyadmin/
```
depois crie o seu banco de dados e a seguir adicione o nome dele na linha `DB_DATABASE=<Seu banco>` do arquivo `.env`

então vamos gerar nossas migrations com o comando:

```
php artisan migrate
```

Pronto! Tudo configurado, pra conseguirmos ver ela rodando basta abrir mais um terminal e fazer o seguinte:

Terminal 1 digite:
```
npm run watch
```
Terminal 2 digite:
```
php artisan serve
```

Vá no browser e digite:
```
http://127.0.0.1:8000
```

## Enjoy Now! ;)
