<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Bill;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class BillController extends Controller
{
    public function index(){

        $user = auth()->user();

        $bills = $user->bills;

        return Inertia::render('Dashboard', ['bills' => $bills]);

    }

    public function create(){
        return Inertia::render('Bill/CreateBill');
    }

    public function store(Request $request){

        // validate
        $this->billValidate($request);

        $bill = new Bill();

        $bill->type = $request->type; 
        $bill->name = $request->name;
        $bill->register = $request->register; 
        $bill->cpf = $request->cpf; 
        $bill->cnpj = $request->cnpj; 
        $bill->due_date = $request->date; 
        $bill->bill_value = strval($request->value);
        $bill->checked = $request->checked;

        $user = auth()->user();
        $bill->user_id = $user->id;
        
        $bill->save();

        return Redirect::route('dashboard')->with('message','Uma nova fatura foi criada!');
    }

    public function destroy($id){

        Bill::findOrFail($id)->delete();

        return Redirect::route('dashboard')->with('message','A fatura foi excluída!');

    }

    public function edit($id){

        $bill = Bill::findOrFail($id);

        return Inertia::render('Bill/EditBill', ['bill' => $bill]);

    }

    public function update(Request $request){

        // validate
        $this->billValidate($request);

        $bill = Bill::findOrFail($request->id);

        $bill->type = $request->type; 
        $bill->name = $request->name;
        $bill->register = $request->register; 
        $bill->cpf = $request->cpf; 
        $bill->cnpj = $request->cnpj; 
        $bill->due_date = $request->date; 
        $bill->bill_value = strval($request->value);
        $bill->checked = $request->checked;

        $bill->save();

        return Redirect::route('dashboard')->with('message','A fatura foi atualizada!');

    }

    public function upChecked(Request $request){

        $msg = '';
        $check = Bill::findOrFail($request->id)->update($request->all());
        $bill = Bill::findOrFail($request->id);

        if($bill->checked == true){
            $msg = 'A fatura foi concluída!';
        }else{
            $msg = 'A fatura foi reaberta!';
        }

        return Redirect::route('dashboard')->with('message', $msg);
    }

    public function billValidate(Request $request){
        // standard validations
        $request->validate([
            'type' => 'required',
            'register' => 'required',
            'date' => 'bail|required|date|after:yesterday',
            'value' => 'bail|required|numeric|between:0,9999999999.99',
        ]);

        // cpf or cnpj validation
        if($request->register == 0){
            $request->validate([
                'cpf' => 'bail|required|formato_cpf|cpf',
            ]);
        }else{
            $request->validate([
                'cnpj' => 'bail|required|formato_cnpj|cnpj',
            ]);
        }
    }
}
