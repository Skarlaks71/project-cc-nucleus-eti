<?php

use App\Http\Controllers\BillController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
})->name('home');

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', [BillController::class, 'index'])->name('dashboard');
    Route::get('/bills/create', [BillController::class, 'create'])->name('createBill');
    Route::post('/bills', [BillController::class, 'store']);
    Route::delete('/bills/{id}', [BillController::class, 'destroy']);
    Route::get('/bills/edit/{id}', [BillController::class, 'edit'])->name('editBill');
    Route::put('/bills/update/{id}', [BillController::class, 'update']);
    Route::put('/bills/check/{id}', [BillController::class, 'upChecked']);
});
